[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-green.svg)](https://www.gnu.org/licenses/gpl-3.0)

# janela


Repositório que gera o https://janela.xyz. Pretendo centralizar informação noticiosa e das atividades políticas a diferentes níveis. Por enquanto, temos apenas um «rss reader» com algumas fontes.

- pt: abrilabril, eco, esquerdanet, expresso, i, je, jne, jno, observador, publico, rr, rtp, sabado, sicnot, tsf, tvi24, visao
- mundo: aljazeera, ap, bbc, cgtn, cnn, economist, elpais, euronews, france24, ft, guardian, intercept, jacobin, lefigaro, lemonde, nyt, politico, rt, spiegel, sputnik, wapo 

![](janela.png)
