#!/usr/bin/env python
# coding: utf-8

print("scraping started")

import pandas as pd
import feedparser
import csv



html_head = """
<head>
  <meta charset="utf-8">
  <meta http-equiv="refresh" content="300">
  <title>janela</title>
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <style>
    
    * {
	box-sizing: border-box;
    }

    :root {
	--background-color: white;
	--color: black;
    }

    @media (prefers-color-scheme: dark) {
	:root {
	    --background-color: black;
	    --color: white;
	}
    }

    body {
	font-family: 'Courier New', monospace;
	background-color: var(--background-color);
	color: var(--color);
    }
    
    .center {
	padding: 32px;
	width: 100%;
    }

    .row {
	margin-left:-5px;
	margin-right:-5px;
    }
    
    .column {
	float: left;
	width: 50%;
	padding: 10px;
    }

    /* Clearfix (clear floats) */
    .row::after {
	content: "";
	clear: both;
	display: table;
    }

    table {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
    }
    
    td {
	vertical-align:top;
	padding:0 5px;
    }
    
    a:link {
	color: var(--color);
	text-decoration: none;
    }
    
  </style>
  <script data-goatcounter="https://janela.goatcounter.com/count"
          async src="//gc.zgo.at/count.js"></script>
</head>
"""

def html_body(a,b): return("""
<body>
  <div class="row center">
    <div class="column">
      <table>
	{}
      </table>
    </div>
    <div class="column">
      <table>
	{}
      </table>
    </div>
  </div>
  <div style="text-align: center;">
    <a href="https://twitter.com/incredivel"><img width="25" style="filter:grayscale(100%);" src="assets/twitter.png"></a>
    <a href="https://gitlab.com/incredivel/janela"><img width="25" style="filter:grayscale(100%);" src="assets/gitlab.ico"></a>
    <a href="https://janela.goatcounter.com/"><img width="25" style="filter:grayscale(100%);" src="assets/goatcounter.png"></a>
  </div>
  <script>
    function mudarCor() {{
	var element = document.body;
	element.classList.toggle("white-mode");
    }}
  </script>
</body>
""".format(a,b))


def getCountry(add, region):
    feeds_links = pd.read_csv("./{}.csv".format(["news","noticias"][region=="pt"]))
    
    f = open("feed_{}.csv".format(add), "w")
    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
    writer.writerow(["site", "time", "title", "link"])
    
    for _ in range(feeds_links.shape[0]):
        name, url = feeds_links.iloc[_]
        for i in feedparser.parse(url)["entries"]:
            title, link, time = [i[x] for x in ["title", "link"]]+[i.get("published", i.get("updated"))]
            if name == "sabado": title = title[10:-4]
            elif name == "rr": time = time[:-2]
            elif name == "i": time = time.replace("T12:","T00:")
            if name == "esquerdanet": time = pd.to_datetime(time[5:]+"+0000", format="%d/%m/%Y - %H:%M%z", utc=True)
            elif name == "wapo": time = pd.to_datetime(time[:-4]+"-05:00", utc=True)
            elif None not in  [name, time, title, link]: 
                time = pd.to_datetime(time, utc=True)
                writer.writerow([name, time, title, link])
    f.close() 

    novo, pt = pd.read_csv("feed_{}.csv".format(add)), pd.read_csv("feed_{}.csv".format(region))
    temp = pd.concat([novo,pt], ignore_index=True).sort_values("time", ascending=False).drop_duplicates(subset=['site', 'title', 'link'], keep='last').dropna()
    temp.time = pd.to_datetime(temp.time)
    temp.loc[temp.time > pd.Timestamp.now(tz="uct"),"time"] = pd.Timestamp.now(tz="uct")
    temp = temp[temp.time>pd.Timestamp.now(tz="uct")-pd.Timedelta(days=1)].reset_index(drop=True)
    temp.to_csv("feed_{}.csv".format(region), index=False)
    return(temp)

df_pt = getCountry("novo", "pt")
df_en = getCountry("new", "en")

def getTable(df): return("".join(df.apply(lambda x: 
                  "<tr><td>{}</td><td><img width=\"16\" src=\"assets/{}.png\" alt=\"{}\"></td><td><a href=\"{}\" target=\"_blank\">{}</a></td></tr>".\
                  format(str(x.time.time())[:5], x.site, x.site, x.link,x.title), axis=1).tolist()))



html = """
<!DOCTYPE html>
<html>
  {}
  {}
</html>
""".format(html_head, html_body(getTable(df_pt),getTable(df_en))).replace("\n","").replace("\t","")



index = open("site/index.html","w")
index.write(html)
index.close()

print("scraping ended")
